# Playbook to install AWX on Centos7

This playbook will install the required services, pull the AWX repo, and install via a plabook from that repo. 


### Prereqs

1. epel installed on client
2. ansible version on host capable of reboot module


### ToDo

1. Create template from AWX inventory
2. Add vars for template
